package com.ero.imageconvert

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import java.io.ByteArrayOutputStream
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.FileUtils
import android.util.Log
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.util.*
import android.R.attr.path
import android.graphics.BitmapFactory
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import kotlinx.android.synthetic.main.activity_main.*
import java.nio.file.Paths


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadImage()
    }

    private fun loadImage(){
        val stream = ByteArrayOutputStream()
        val drawable = ContextCompat.getDrawable(this@MainActivity, R.drawable.image)
        val bitmap = (drawable as BitmapDrawable).bitmap
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val bitMapData = stream.toByteArray()

        Log.e("BYTEARRAY", Arrays.toString(bitMapData))

        val bMap = BitmapFactory.decodeByteArray(bitMapData, 0, bitMapData.size)

        val file = File(
            Environment.getExternalStorageDirectory().toString() + "/DCIM/",
            "image" + Date().time + ".png"
        )

        val output = FileOutputStream(file)
        bMap.compress(Bitmap.CompressFormat.JPEG, 100, output)
        output.flush()
        output.close()

        img_encode.setImageBitmap(bMap)
        Log.e("PATH123", file.absolutePath)
    }
}
